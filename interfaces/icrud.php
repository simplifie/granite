<?php
namespace simplifie;
interface ICrud
{
  //Since this is a library, let's create
  //a separate DB connection using PDO.
  function setConn($host, $db, $username, $password = '');
  function getConn();
  //
  function create($data);
  function read($id);
  function readBy($fieldName, $value);
  function update($id, $data);
  function updateBy($fieldName, $value, $data);
  function delete($id);
  function deleteBy($fieldName, $value);
}