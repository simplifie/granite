<?php
namespace simplifie;
interface IIdentity
{
  function setId($id);
  function getId();
}