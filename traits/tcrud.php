<?php
/**
 * Repository pattern for child models.
 * Just set the name of the table you want to
 * use before performing CRUD operations.
 *
 * <code>
 * $this->setTableName('users');
 * $this->create($data);
 * </code>
 *
 */
namespace simplifie;
use PDO;
trait TCrud
{
  private $sTableName;
  private $conn;
  protected final function setConn(
    $host = 'localhost',
    $db = '',
    $username = 'root',
    $password = ''
  )
  {
    $this->conn = new PDO(
      "mysql:host=$host;dbname=$db",
      $username,
      $password
    );
  }
  protected final function getConn()
  {
    return $this->conn;
  }
  private final function query($query)
  {
    //TODO: Prevent SQL injection and enable err mode.
    //$this->getConn()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $s = $this->getConn()->prepare($query);
    $s->execute();
    $s->setFetchMode(PDO::FETCH_OBJ);
    return $s->fetchAll();
  }
  //
  public final function getTableName()
  {
    return $this->sTableName;
  }
  public final function setTableName($tableName)
  {
    $this->sTableName = $tableName;
  }
  private final function getUpdateSet($data)
  {
    $a = [];
    foreach($data as $key => $value)
    {
      array_push($a, "$key='$value'");
    }
    return implode(", ", array_values($a));
  }
  //
  public final function all()
  {
    $s = "SELECT * FROM {$this->getTableName()}";
    return $this->query($s);
  }
  public function create($data)
  {
    $k = implode(", ", array_keys($data));
    $v = implode("', '", array_values($data));
    $s = "INSERT INTO {$this->getTableName()} ($k) VALUES ($v)";
    return $this->query($s);
  }
  public function read($id)
  {
    $s = "SELECT * FROM {$this->getTableName()} WHERE id = '$id'";
    return $this->query($s);
  }
  public function readBy($fieldName, $value)
  {
    $s = "SELECT * FROM {$this->getTableName()} WHERE $fieldName = '$value'";
    return $this->query($s);
  }
  public function update($id, $data)
  {
    $t = $this->getUpdateSet($data);
    $s = "UPDATE {$this->getTableName()} SET $t WHERE id = '$id'";
    return $this->query($s);
  }
  public function updateBy($fieldName, $value, $data)
  {
    $t = $this->getUpdateSet($data);
    $s = "UPDATE {$this->getTableName()} SET $t WHERE $fieldName = '$value'";
    return $this->query($s);
  }
  public function delete($id)
  {
    $s = "DELETE FROM {$this->getTableName()} WHERE id = '$id'";
    return $this->query($s);
  }
  public function deleteBy($fieldName, $value)
  {
    $s = "DELETE FROM {$this->getTableName()} WHERE $fieldName = '$value'";
    return $this->query($s);
  }
}