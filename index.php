<?php
require_once 'vendor/autoload.php';
$m = new simplifie\SomeModel();
$m->setId(2);
//CRUD.
$m->setTableName('users');
print_r($m->read($m->getId()));
//Auth.
echo $m->hasRoles(['Super Admin', 'Admin']);
echo $m->isPermitted(['User']);
echo $m->isPermitted(['User', 'Not Permitted X']);
echo $m->isPrivileged(['User'], ['Create']);
echo $m->isPrivileged(['Auth Login'], ['Read']);
echo $m->isPrivileged(['Auth Login'], ['Not Permitted X']);