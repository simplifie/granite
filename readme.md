# Project Granite
#### by Simplifie

A collection of useful generic classes, traits and interfaces independent of any structure or framework.

## Installation

```
composer require simplifie/granite
```

## Usage

Stop re-creating authentications and CRUD methods over and over again especially when using native PHP codes.

* Refer to the samples found in vendor/simplifie/granite/index.php file
* Also refer to the sample model implementation in vendor/simplifie/granite/models/somemodel.php
* Refer to the SQL files found in vendor/simplifie/granite/models directory when doing migrations
* In cases when you're using a framework, just link it to the vendor/autoload.php file
	
##### CRUD


	class SomeModel {use simplifie\TCrud; }
	class SomeCtrl
	{
		$m->setTableName('users');
		print_r($m->read(2));
	}


##### Auth

	class SomeModel { use simplifie\TAuth; }
	class SomeCtrl
	{
		$m->setId(2);
		echo $m->isPermitted(['User']);
	}

## Contributors

* [Mark N. Amoin](mailto:prezire@gmail.com)