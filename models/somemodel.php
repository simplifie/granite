<?php
namespace simplifie;
use simplifie\TAuth     as TAuth;
use simplifie\TCrud     as TCrud;
use simplifie\IIdentity as IIdentity;
use simplifie\ICrud     as ICrud;
final class SomeModel implements IIdentity, ICrud
{
  public function __construct(
    $host = 'localhost',
    $db = 'test',
    $username = 'root',
    $password = ''
  )
  {
    $this->setConn(
      $host,
      $db,
      $username,
      $password
    );
  }
  use TAuth;
  use TCrud;
}